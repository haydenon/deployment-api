FROM docker:git
ARG CLOUD_SDK_VERSION=347.0.0-linux-x86_64

RUN apk add --no-cache python3
RUN wget https://dl.google.com/dl/cloudsdk/channels/rapid/downloads/google-cloud-sdk-${CLOUD_SDK_VERSION}.tar.gz
RUN tar xzf google-cloud-sdk-${CLOUD_SDK_VERSION}.tar.gz
RUN mv ./google-cloud-sdk /opt/google-cloud-sdk
RUN /opt/google-cloud-sdk/install.sh --usage-reporting=false --quiet

ENV PATH="${PATH}:/opt/google-cloud-sdk/bin"
