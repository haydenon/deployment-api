using System;

namespace DeploymentExample
{
    public record Cat(Guid Id, string Name, string PhotoUri);
}
