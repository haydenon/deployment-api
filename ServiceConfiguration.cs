using Google.Cloud.Firestore;

namespace DeploymentApi
{
    [FirestoreData]
    public class ServiceConfiguration
    {
        [FirestoreProperty("projectId")]
        public string ProjectId { get; set; }

        [FirestoreProperty("region")]
        public string Region { get; set; }

        [FirestoreProperty("serviceName")]
        public string ServiceName { get; set; }

        [FirestoreProperty("serviceUrl")]
        public string ServiceUrl { get; set; }

        [FirestoreProperty("repository_base")]
        public string RepositoryBase { get; set; }

        [FirestoreProperty("repository_name")]
        public string RepositoryName { get; set; }
    }
}