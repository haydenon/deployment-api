using Google.Cloud.Firestore;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Threading.Tasks;
using Microsoft.IdentityModel.Protocols.OpenIdConnect;
using System.Threading;
using Microsoft.IdentityModel.Protocols;
using Microsoft.AspNetCore.Authorization;

namespace DeploymentApi
{
  public class Startup
  {
    public Startup(IConfiguration configuration)
    {
      Configuration = configuration;
    }

    public IConfiguration Configuration { get; }

    private static readonly string PublicAuthorizationPolicy = "PublicPolicy";

    const string GoogleDomain = "accounts.google.com";
    private static Lazy<Task<OpenIdConnectConfiguration>> OpenIdConfig = new Lazy<Task<OpenIdConnectConfiguration>>(
        async () =>
        {
          var configurationManager =
                      new ConfigurationManager<OpenIdConnectConfiguration>(
                          $"https://{GoogleDomain}/.well-known/openid-configuration",
                          new OpenIdConnectConfigurationRetriever());
          return await configurationManager.GetConfigurationAsync(CancellationToken.None);
        }
    );


    public void ConfigureServices(IServiceCollection services)
    {
      var dbConfig = Configuration.GetSection("Database");
      var db = FirestoreDb.Create(dbConfig["ProjectId"]);
      services.AddSingleton(db);

      services.AddControllers();

      var audience = Configuration.GetSection("OAuth").GetValue<string>("ClientId");
      var openIdConfig = OpenIdConfig.Value.Result;

      services.AddAuthentication(opts =>
          opts.DefaultAuthenticateScheme
            = opts.DefaultScheme
            = opts.DefaultChallengeScheme
            = JwtBearerDefaults.AuthenticationScheme
      ).AddJwtBearer(opts =>
      {
        opts.RequireHttpsMetadata = false;
        opts.SaveToken = true;
        var validationParameters = new TokenValidationParameters
        {
          ValidIssuer = GoogleDomain,
          ValidAudiences = new[] { audience },
          IssuerSigningKeys = openIdConfig.SigningKeys
        };

        opts.TokenValidationParameters = validationParameters;
      });
      services.AddAuthorization(options =>
      {
        options.AddPolicy(
            PublicAuthorizationPolicy,
            new AuthorizationPolicyBuilder()
                .RequireAssertion(_ => true)
                .Build());

        options.FallbackPolicy = new AuthorizationPolicyBuilder()
            .RequireAuthenticatedUser()
            .Build();
      });
    }

    // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
    public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
    {
      if (env.IsDevelopment())
      {
        app.UseDeveloperExceptionPage();
      }

      app.UseRouting();
      app.UseAuthentication();
      app.UseAuthorization();
      app.UseEndpoints(endpoints =>
      {
        endpoints.MapControllers();
      });
    }
  }
}
