using System.Text.Json;
using Google.Cloud.Firestore;

namespace DeploymentApi
{
  [FirestoreData]
  public class DeployUserKey
  {
    [FirestoreProperty("key")]
    public string Key { get; set; }

    public Key KeyValue => JsonSerializer.Deserialize<Key>(Key);
  }

  public class Key
  {
    public string type { get; set; }
    public string project_id { get; set; }
    public string private_key_id { get; set; }
    public string private_key { get; set; }
    public string client_email { get; set; }
    public string client_id { get; set; }
    public string auth_uri { get; set; }
    public string token_uri { get; set; }
    public string auth_provider_x509_cert_url { get; set; }
    public string client_x509_cert_url { get; set; }
  }
}