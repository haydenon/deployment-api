﻿using System.Linq;
using System.Threading.Tasks;
using Google.Cloud.Firestore;
using Microsoft.AspNetCore.Mvc;

namespace DeploymentApi.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class InfrastructureController : ControllerBase
    {
        private const string EmailClaim = "http://schemas.xmlsoap.org/ws/2005/05/identity/claims/emailaddress";

        private readonly FirestoreDb db;

        public InfrastructureController(FirestoreDb db)
        {
            this.db = db;
        }

        [HttpGet]
        [Route("service/configuration")]
        public async Task<ActionResult> GetServiceConfig()
        {
            var key = $"serviceConfig_{EmailAddress}";
            var snapshot = await db.Collection("serviceConfig").Document(key).GetSnapshotAsync();
            if (!snapshot.Exists)
            {
                return new NotFoundObjectResult("Service config does not exist.");
            }

            return new JsonResult(snapshot.ConvertTo<ServiceConfiguration>());
        }

        [HttpGet]
        [Route("deploy-user/key")]
        public async Task<ActionResult> GetDeployUserKey()
        {
            var docKey = $"deployKey_{EmailAddress}";
            var snapshot = await db.Collection("deploySaKeys").Document(docKey).GetSnapshotAsync();
            if (!snapshot.Exists)
            {
                return new NotFoundObjectResult("Deploy user key does not exist.");
            }

            var key = snapshot.ConvertTo<DeployUserKey>();

            return new JsonResult(key.KeyValue);
        }

        private string EmailAddress
          => HttpContext.User.Claims.First(c => c.Type == EmailClaim).Value;
    }
}
