using System.Linq;
using System.Net.Http;
using System.Text.Json;
using System.Threading.Tasks;
using DeploymentApi;
using DeploymentExample;
using Google.Cloud.Firestore;
using Microsoft.AspNetCore.Mvc;

namespace deployment_api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class CatController : ControllerBase
    {
        private const string EmailClaim = "http://schemas.xmlsoap.org/ws/2005/05/identity/claims/emailaddress";
        private static readonly HttpClient client = new HttpClient();
        private static readonly JsonSerializerOptions serializerOptions = new JsonSerializerOptions
        {
            PropertyNameCaseInsensitive = true
        };

        private readonly FirestoreDb db;

        public CatController(FirestoreDb db)
        {
            this.db = db;
        }

        [HttpGet]
        [Route("random")]
        public async Task<ActionResult<Cat>> GetRandomCat()
        {
            var key = $"serviceConfig_{EmailAddress}";
            var snapshot = await db.Collection("serviceConfig").Document(key).GetSnapshotAsync();
            if (!snapshot.Exists)
            {
                return new NotFoundObjectResult("Service config does not exist.");
            }
            var config = snapshot.ConvertTo<ServiceConfiguration>();
            var url = config.ServiceUrl;
            var request = new HttpRequestMessage(HttpMethod.Get, $"{url}/cat/random");
            var token = HttpContext.Request.Headers["Authorization"].First();
            request.Headers.Add("Authorization", token);
            var response = await client.SendAsync(request);

            if (!response.IsSuccessStatusCode)
            {
                return StatusCode((int)response.StatusCode);
            }

            if (response.Content.Headers.ContentType.MediaType != "application/json")
            {
                return NotFound();
            }

            var content = await response.Content.ReadAsStringAsync();

            var cat = JsonSerializer.Deserialize<Cat>(content, serializerOptions);
            return Ok(cat);
        }

        private string EmailAddress
          => HttpContext.User.Claims.First(c => c.Type == EmailClaim).Value;
    }
}